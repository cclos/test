<img src="https://cclos-assets.s3.us-east-1.amazonaws.com/images/Logo%20credit.png" alt="CCLOS Logo" width="200"/>

# Admission Test

We are currently looking for developers. Are you interested in financial solutions and software? Your dream is joining a passionate crew? Do you enjoy challenges and learning on a daily basis? If you are looking to boost your career as a software engineer, then CCLOS may be the place for you.

### Tests:

- [Backend Developer Test](backend/README.md)
