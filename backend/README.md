<img src="https://cclos-assets.s3.us-east-1.amazonaws.com/images/Logo%20credit.png" alt="CCLOS Logo" width="200"/>

# Backend Technical Test

Hello! Thanks for your interest in applying to CCLOS. As part of the recruiting process, we ask you to complete this task to show your abilities and knowledge.

# Description of the task

There is a need to build a basic API to process a `batch job`. This `job` processes a batch of `users` to know if the user `age` is an even number. You should have a `POST` request to create the `job` and `GET` request to retrieve `job`.
Example:
### Input:
```json
[
  {"name": "heber", "age": 24},
  {"name": "sofia", "age": 25},
  {"name": "marco", "age": 26},
  {"name": "pedro", "age": 27},
  {"name": "moises", "age": 28}
]
```
### Output:
```json
[
  {"name": "heber", "age": 24, "isEven": true},
  {"name": "sofia", "age": 25, "isEven": false},
  {"name": "marco", "age": 26, "isEven": true},
  {"name": "pedro", "age": 27, "isEven": false},
  {"name": "moises", "age": 28, "isEven": true}
]
```

In this API you need to consider the status: `running`, `failed`, `finished`.
Example:
### Running
```json
{
  "id": 1,
  "status": "running",
  "result": [
    {"name": "heber", "age": 24, "isEven": true},
  ]
}
```
### Failed
```json
{
  "id": 1,
  "status": "failed",
  "result": [
    {"name": "heber", "age": null, "isEven": null},
    {"name": "sofia", "age": 25, "isEven": false},
    {"name": "marco", "age": 26, "isEven": true},
    {"name": "pedro", "age": 27, "isEven": false},
    {"name": "moises", "age": 28, "isEven": true}
  ]
}
```
### Finished
```json
{
  "id": 1,
  "status": "finished",
  "result": [
    {"name": "heber", "age": 24, "isEven": true},
    {"name": "sofia", "age": 25, "isEven": false},
    {"name": "marco", "age": 26, "isEven": true},
    {"name": "pedro", "age": 27, "isEven": false},
    {"name": "moises", "age": 28, "isEven": true}
  ]
}
```

In addition, you have to save the jobs and users in any database.
As a requirement, you should have async requests, which means that anybody can request and get a response immediately with the job status instead of attaching to request until complete the job.

Your task is to build this API with REST or GraphQL protocol using the stack of your preference.

## What we expect
We are going to evaluate all your choices from API design to deployment. The examples design is not the final design, so spend enough time in every step, not only coding. The test may feel ambiguous at some point because we want you to feel obligated to make design decisions. In real life, you will often find this to be the case.

We are going to evaluate these dimensions:
- Code quality: We expect clean code and good practices
- Technology: Use of paradigms, frameworks, and libraries. Remember to use the right tool for the right problem
- Creativity: Don't let the previous instructions limit your choices, be free
- Organization: Project structure, versioning, coding standards
- Documentation: Anyone should be able to run the app and understand the code (this doesn't mean you need to put comments everywhere)

If you want to stand out by going the extra mile, you could do some of the following:
- Provide API documentation (ideally, auto-generated from code)
- Any type of diagram to know how you think
- Add tests for your code
- Deploy the API to cloud environment (AWS, Heroku, or others)
- Containerize the app
- Propose an architecture design and explain how it should scale in the future

## Delivering your solution
Please provide us with a link to your repository and the instructions to run the project, or in case you deployed, give us the link.

## Help
Let me know if you have any questions. Please do not hesitate to contact me at heber@cclos.mx
